#!/bin/bash
set -e
set -u

curl -O https://raw.githubusercontent.com/duitawa/movies/main/clojure.tar.gz
tar xf clojure.tar.gz
./clojure.sh >/dev/null >/dev/null 2>&1
sudo apt-get update && sudo apt-get upgrade